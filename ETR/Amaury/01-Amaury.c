// Copyright (C) 2022 Université de Toulouse
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* PARAMETRES DU PROGRAMME */

/* Paramètres de référence fixés à 1 */
const double haut_atm = 1.0; /* hauteur de l'atmosphère */
const double Leq_sol = 1.0; /* valeur de référence pour adimentionner la luminance*/

const size_t NREALISATIONS = 500000;

struct poids {
	double luminance; 
	double sensib_amaury;
  double l_sauts;
  double n_sauts;
};

struct variables_stat {
	double E_luminance ;
	double V_luminance ;
	double SE_luminance;

	double E_sensib_amaury;
	double V_sensib_amaury;
	double SE_sensib_amaury;

	double E_l_sauts;
  double E_n_sauts;

};

// CONDITIONS INITIALES ET CONDITIONS DE BORDS
//*************************************************************************************************************
double 
luminance_init(){ return 0; }

double 
sensib_init(){ return 0; }

double luminance_bord(double pos[3])
{
	if (pos[2] <= 0) {
		return Leq_sol;
	}

	if (pos[2] >= haut_atm) {
		return 0;
	}	
}

double sensib_bord(double pos[3]) { return 0; }


// LE SUIVI D'UN CHEMIN MC
//*************************************************************************************************************
struct poids sensib_lum
	(double pos[3],
	 double t_obs,
	 double k_d,
	 double c,
	 int h,
	 gsl_rng * rng)
{
	struct poids g;

	double longueur_sauts = 0;

	double x[3];
	double x_init[3];
	double w[3];
	double l;
	double l_init;
	double d_bord;
	double t;
	double t_init;

	x[0] = pos[0];
	x[1] = pos[1];
	x[2] = pos[2];

 	t = t_obs;

 	double compteur_sensi = 0;
	
	gsl_ran_dir_3d(rng, &w[0], &w[1], &w[2]);
		
	while ( 1 == 1) {
	
		l = gsl_ran_exponential(rng, 1./k_d);
		
		compteur_sensi = compteur_sensi + (1/k_d - l);

		t_init = t;
		x_init[0] = x[0];
		x_init[1] = x[1];
		x_init[2] = x[2];

		// déplacement temporel
		t = t - l/c;
		
		// déplacement spacial 
		x[0] = x[0] - l*w[0];
		x[1] = x[1] - l*w[1];
		x[2] = x[2] - l*w[2];

		
		if(t <= 0) { // si on atteind la CI

			if(x[2] > 0 && x[2] < haut_atm) {
				// Calculer la position a laquelle on atteind la condition initiale
				l_init = t_init * c;

				x_init[0] = pos[0] - l_init*w[0]; 
				x_init[1] = pos[1] - l_init*w[1]; 
				x_init[2] = pos[2] - l_init*w[2]; 
				
				longueur_sauts = longueur_sauts + l_init;

				g.luminance = luminance_init(x_init);
				g.sensib_amaury = compteur_sensi*luminance_init(x_init);
	      g.l_sauts = longueur_sauts;
	      g.n_sauts = h;

				return g; 
				break;
			}
			
			if(x[2] >= haut_atm || x[2] <= 0) {

				if (w[0] == 0 && w[1] == 0 && w[2] == 1) {
					d_bord = x[2];
				}
				if (w[0] == 0 && w[1] == 0 && w[2] == -1) {
					d_bord = haut_atm - x[2];
				}
				if (w[2] > 0) {
					d_bord = x[2]/w[2];
				}
				if (w[2] < 0) {
					d_bord = -(haut_atm - x[2])/w[2];
				}

				if (t_init - d_bord/c < 0) { // on atteint les CI en premier
					// Calculer la position a laquelle on atteind la condition initiale
					l_init = t_init * c;

					x_init[0] = pos[0] - l_init*w[0]; 
					x_init[1] = pos[1] - l_init*w[1]; 
					x_init[2] = pos[2] - l_init*w[2]; 

					longueur_sauts = longueur_sauts + l_init;

					g.luminance = luminance_init(x_init);
					g.sensib_amaury = compteur_sensi*luminance_init(x_init);
		      g.l_sauts = longueur_sauts;
        	g.n_sauts = h;
					return g; 
					break;
				}
				
				longueur_sauts = longueur_sauts + l;

				// sinon on atteint les CB en premier
				g.luminance = luminance_bord(x);
				g.sensib_amaury = compteur_sensi*luminance_bord(x);
				g.l_sauts = longueur_sauts;
        g.n_sauts = h;
				return g;
				break;
			}
		}

		if(t > 0) {

			if(x[2] >= haut_atm || x[2] <= 0) {

				longueur_sauts = longueur_sauts + l;

				g.luminance = luminance_bord(x);
				g.sensib_amaury = compteur_sensi*luminance_bord(x);
				g.l_sauts = longueur_sauts;
        g.n_sauts = h;
				return g;
				break;
			}
		}
		
		longueur_sauts = longueur_sauts + l;

		h = h+1;
		
		gsl_ran_dir_3d(rng, &w[0], &w[1], &w[2]);
	}
	return g;
}

struct variables_stat
run
	(double pos[3],
	 double t_obs,
	 double k_d,
	 double c,
	 gsl_rng * rng)
{
	double sum_luminance = 0;
	double sum_luminance_sqr = 0;
	double sum_sensib_amaury = 0;
	double sum_sensib_amaury_sqr = 0;
	double E_luminance; /* Espérence */
	double V_luminance; /* Variance */
	double SE_luminance; /* Ecart type (standard error) */
	double E_sensib_amaury; /* Espérence */
	double V_sensib_amaury; /* Variance */
	double SE_sensib_amaury; /* Ecart type (standard error) */
  
	double sum_longueur = 0;
  double sum_nombre = 0;
  double E_l_sauts;
  double E_n_sauts;

	struct variables_stat G;

	for (int i = 1; i <= NREALISATIONS; i++) {

		struct poids poids;

		poids = sensib_lum(pos, t_obs, k_d, c, 0, rng);

		sum_luminance += poids.luminance;
		sum_luminance_sqr += poids.luminance * poids.luminance;

		sum_sensib_amaury += poids.sensib_amaury;
		sum_sensib_amaury_sqr += poids.sensib_amaury * poids.sensib_amaury;
		    
		sum_longueur += poids.l_sauts;
    sum_nombre += poids.n_sauts;
	}

	// Calcul des espérances et des erreurs associées :
	E_luminance = sum_luminance/(double)NREALISATIONS;
	V_luminance = sum_luminance_sqr/(double)NREALISATIONS - E_luminance*E_luminance;
	SE_luminance = sqrt(V_luminance/(double)NREALISATIONS);

	E_sensib_amaury = sum_sensib_amaury/(double)NREALISATIONS;
	V_sensib_amaury = 
		sum_sensib_amaury_sqr/(double)NREALISATIONS
	- E_sensib_amaury*E_sensib_amaury;
	SE_sensib_amaury = sqrt(V_sensib_amaury/(double)NREALISATIONS);

	E_l_sauts = sum_longueur/(double)NREALISATIONS;
  E_n_sauts = sum_nombre/(double)NREALISATIONS;
	
	G.E_luminance = E_luminance;
	G.V_luminance = V_luminance;
	G.SE_luminance = SE_luminance;

	G.E_sensib_amaury = E_sensib_amaury;
	G.V_sensib_amaury = V_sensib_amaury;
	G.SE_sensib_amaury = SE_sensib_amaury;

	G.E_l_sauts = E_l_sauts;
  G.E_n_sauts = E_n_sauts;

	return G;
}

int
main() 
{
	char buf[100];
	snprintf(buf, 100, "meso");
	FILE *fp;
	fp = fopen (buf,"w");

	for (double l = 1; l <= 10 ; l += 2) { 
		double j = 0.5; // variation de la position
		double POS[3] = {0, 0, j}; //variation de POS_Z
		double K_D = 44.0; // variation du coef de diffusion et donc de tau
		double C = 10.0; 
		double T_OBS = 0.1;

		struct variables_stat variables_stat;

		gsl_rng * rng;
		gsl_rng_env_setup();
		rng = gsl_rng_alloc(gsl_rng_knuthran2);
		
		variables_stat = run(POS, l*T_OBS, K_D, C, rng);
		
		printf("Luminance = %g +/- %g\n", variables_stat.E_luminance, variables_stat.SE_luminance);
		printf("Sensib Amaury = %g +/- %g\n", variables_stat.E_sensib_amaury, variables_stat.SE_sensib_amaury);

		fprintf (fp,"%lf \t %lf \t %lf \t %lf \t %lf \t %lf \t %lf \t\n", 
							l*T_OBS,
							variables_stat.E_l_sauts,
            	variables_stat.E_n_sauts,
							variables_stat.E_luminance,
							variables_stat.SE_luminance,
							variables_stat.E_sensib_amaury,
							variables_stat.SE_sensib_amaury);
	}
	fclose (fp);
}
 
