// Copyright (C) 2022 Université de Toulouse
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* PARAMETRES DU PROGRAMME */

/* Paramètres de référence */
const double haut_atm = 1.0; /* hauteur de l'atmosphère */
const double T_sol = 1.0; /* valeur de référence pour adimentionner la luminance*/
const double delta = 0.01; /* longueur du saut spatial */

const size_t NREALISATIONS = 100000;

struct poids {
	double T; 
	double sensib_amaury;
	double l_sauts;
 	double n_sauts;
};

struct variables_stat {
	double E_T ;
	double V_T ;
	double SE_T;

	double E_sensib_amaury;
	double V_sensib_amaury;
	double SE_sensib_amaury;

	double E_l_sauts;
	double E_n_sauts;
};

// CONDITIONS INITIALES ET CONDITIONS DE BORDS
//*************************************************************************************************************
double 
T_init(){ return 0; }

double 
sensib_init(){ return 0; }

double T_bord(double pos[3])
{
	if (pos[2] <= 0) {
		return T_sol;
	}

	if (pos[2] >= haut_atm) {
		return 0;
	}	
}

double sensib_bord(double pos[3]) { return 0; }


// LE SUIVI D'UN CHEMIN MC
//*************************************************************************************************************
struct poids sensib_T
	(double pos[3],
	 double t_obs,
	 double k_t,
	 double c,
	 double k_d,
	 int h,
	 gsl_rng * rng)
{
	struct poids g;

	double longueur_sauts = 0;
	
	double x[3];
	double w[3];
	double t;
	double s;

	x[0] = pos[0];
	x[1] = pos[1];
	x[2] = pos[2];

 	t = t_obs;

	double corr_sensi_k_d = -2*c/(delta*delta*k_d*k_d); 
 	double compteur_sensi = 0;

	while ( 1 == 1) {

		// tirage d'un temps s selon la pdf exp
		s = gsl_ran_exponential(rng, 1./k_t);

		// On remonte le temps
		t = t - s;

		compteur_sensi = compteur_sensi + (1/k_t - s);

		if(t < 0) { // si on atteind la CI
			g.T = T_init(x);
			g.sensib_amaury = compteur_sensi*T_init(x)*corr_sensi_k_d;
			g.l_sauts = longueur_sauts;
			g.n_sauts = h;
			return g; 
			break;
		}

		h = h+1;

		// On échantillonne uniformément la sphère pour définir la prochaine direction après diffusion 
		gsl_ran_dir_3d(rng, &w[0], &w[1], &w[2]);

		// déplacement spacial 
		x[0] = x[0] + delta*w[0];
		x[1] = x[1] + delta*w[1];
		x[2] = x[2] + delta*w[2];
		
		longueur_sauts = longueur_sauts + delta;

		if (x[2] <= 0 || x[2] >= haut_atm) {
			g.T = T_bord(x);
			g.sensib_amaury = compteur_sensi*T_bord(x)*corr_sensi_k_d;
			g.l_sauts = longueur_sauts;
			g.n_sauts = h;
			return g;
			break;
		}
	}
	return g;
}

struct variables_stat
run
	(double pos[3],
	 double t_obs,
	 double k_t,
	 double c,
	 double k_d,
	 gsl_rng * rng)
{
	double sum_T = 0;
	double sum_T_sqr = 0;
	double sum_sensib_amaury = 0;
	double sum_sensib_amaury_sqr = 0;
	double E_T; /* Espérence */
	double V_T; /* Variance */
	double SE_T; /* Ecart type (standard error) */
	double E_sensib_amaury; /* Espérence */
	double V_sensib_amaury; /* Variance */
	double SE_sensib_amaury; /* Ecart type (standard error) */
	
	double sum_l_sauts = 0;
	double sum_n_sauts = 0;
	double E_l_sauts;
	double E_n_sauts;

	struct variables_stat G;

	for (int i = 1; i <= NREALISATIONS; i++) {

		struct poids poids;

		poids = sensib_T(pos, t_obs, k_t, c, k_d, 0, rng);

		sum_T += poids.T;
		sum_T_sqr += poids.T * poids.T;

		sum_sensib_amaury += poids.sensib_amaury;
		sum_sensib_amaury_sqr += poids.sensib_amaury * poids.sensib_amaury;

		sum_l_sauts += poids.l_sauts;
		sum_n_sauts += poids.n_sauts;
	}

	// Calcul des espérances et des erreurs associées :
	E_T = sum_T/(double)NREALISATIONS;
	V_T = sum_T_sqr/(double)NREALISATIONS - E_T*E_T;
	SE_T = sqrt(V_T/(double)NREALISATIONS);

	E_sensib_amaury = sum_sensib_amaury/(double)NREALISATIONS;
	V_sensib_amaury = 
		sum_sensib_amaury_sqr/(double)NREALISATIONS
	- E_sensib_amaury*E_sensib_amaury;
	SE_sensib_amaury = sqrt(V_sensib_amaury/(double)NREALISATIONS);

	E_l_sauts = sum_l_sauts/(double)NREALISATIONS;
	E_n_sauts = sum_n_sauts/(double)NREALISATIONS;

	G.E_T = E_T;
	G.V_T = V_T;
	G.SE_T = SE_T;

	G.E_sensib_amaury = E_sensib_amaury;
	G.V_sensib_amaury = V_sensib_amaury;
	G.SE_sensib_amaury = SE_sensib_amaury;
	
	G.E_l_sauts = E_l_sauts;
	G.E_n_sauts = E_n_sauts;

	return G;
}

int
main() 
{
	char buf[100];
	snprintf(buf, 100, "macro_sansvariationkt");
	FILE *fp;
	fp = fopen (buf,"w");
	
	for (double l = 1; l <= 10 ; l += 2) { 
		double j = 0.5;
		double pos[3] = {0, 0, j};
		double c = 10.0;
		double k_d = 40.0;
		double D = (1.0/3.0)*(c/k_d); 
		double tau = (delta*delta)/(6*D);
		double k_t = 1/tau;
		double t_obs = 0.5;

		struct variables_stat variables_stat;

		gsl_rng * rng;
		gsl_rng_env_setup();
		rng = gsl_rng_alloc(gsl_rng_knuthran2);

		variables_stat = run(pos, l*t_obs, k_t, c, k_d, rng);

		printf("T = %g +/- %g\n", variables_stat.E_T, variables_stat.SE_T);
		printf("Sensib Amaury = %g +/- %g\n", variables_stat.E_sensib_amaury, variables_stat.SE_sensib_amaury);

		fprintf (fp,"%lf \t %lf \t %lf \t %lf \t %lf \t %lf \t %lf \t \n", 
						l*t_obs,
						variables_stat.E_l_sauts,
						variables_stat.E_n_sauts,
						variables_stat.E_T,
						variables_stat.SE_T,
						variables_stat.E_sensib_amaury,
						variables_stat.SE_sensib_amaury);

	}
	fclose (fp);

}

